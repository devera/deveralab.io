(function() { // Wrap in an IIFE

  angular
    .module('devera', ['ngMessages'])
    .controller('contactCtrl', contactCtrl);
	
   contactCtrl.$inject = ['$scope', '$http'];

  function contactCtrl($scope, $http) {

		
	  $scope.submitForm = function() {
		  var dataObj = {
			"entry.1462734466" : name,
			"entry.1464666778" : $scope.school,
			"entry.429907712" : $scope.email,
			"entry.1094769218" : $scope.contactNo,
			"entry.1832440052" : $scope.dob,
			"entry.330431114" : $scope.experience,
			"entry.773280627" : $scope.language,
			"entry.356354267" : $scope.frameworkUsed
		  };
		  
		
		$http({
		  method  : 'POST',
		  url     : 'https://docs.google.com/a/smu.edu.sg/forms/d/e/1FAIpQLScFNrWWcbzo72mCo-fDc-dEqadtCifF7gC0SxuhpyETmpKamQ/formResponse',
		  data    : dataObj,
		  headers : {'Content-Type': 'undefined', 'Access-Control-Allow-Origin': '*'},
		  

		})
		.success(function(data) {
		  console.log(data);

		if (!data.success) {
		  // if not successful, bind errors to error variables
		  //$scope.errorName = data.errors.name;
		  //$scope.errorSuperhero = data.errors.superheroAlias;
		} else {
		  // if successful, bind success message to message
		  $scope.name = "";
		  $scope.school = "";
		  $scope.email = "";
		  $scope.contactNo = "";
		  $scope.dob = "";
		  $scope.experience = "";
		  $scope.language = "";
		  $scope.framework = "";
		  $scope.frameworkUsed = "";
		}
		
		});
	  };
  
  }
	
  

})();
